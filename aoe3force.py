#!/usr/bin/python

import time
from tkinter import *
from tkinter import filedialog
from subprocess import check_output, call
import re
import os
import configparser
from os.path import expanduser
import logging

logging.basicConfig(level=logging.DEBUG)

class IPManager:

    def __init__(self):
        self.ip_addresses = self.get_ips()
    
    def get_ips(self):
        int_ip_correlation = {}
        current_int = ""
        int_ip_correlation = {}
        if os.name == 'nt':  # Window OS
            output_b = check_output(["ipconfig"])
            output = output_b.decode('utf-8', 'replace')
            for line in output.splitlines():
                if not line.startswith(" ") and line.endswith(":"):
                    current_int = line[:-1]
                elif current_int and line.strip().startswith("IPv4 Address"):
                    ip = line.split(":")[1].strip()
                    int_ip_correlation[current_int] = ip
                    current_int = ""
            if current_int:
                int_ip_correlation[current_int] = "NO IP"
        elif os.name == 'posix':  # Expect Linux OS
            output_b = check_output(["ip", "addr"])
            output = output_b.decode('utf-8', 'replace')
            for line in output.splitlines():
                if not line.startswith(" "):
                    current_int = line.split(" ")[1][:-1]
                elif current_int and line.strip().startswith("inet "):
                    line_split = line.strip().split(" ")
                    ip = line_split[1].split("/")[0]
                    int_ip_correlation[current_int] = ip
                    current_int = ""
        return int_ip_correlation

class GameManager:

    def __init__(self, ConfigManager):
        self.cm = ConfigManager

    def start_game(self):
        logging.info("Starting game!")
        root_dir = selected_dir.get("1.0", END).strip()
        game_file = self.name_to_file(variable2.get())
        logging.debug("Game file: %s" % game_file)
        game_exec = os.path.join(root_dir, game_file)
        ip = variable.get().split(" - ")[0].strip()
        logging.debug("IP: %s, PATH: %s" % (ip, game_exec))
        self.cm.save()
        call('"%s" OverrideAddress="%s"' % (game_exec, ip), shell=True)
    
    def file_to_name(self, file):
        return {
            "age3.exe": "Age of Empires III",
            "age3x.exe": "Age of Empires III - The Warchiefs",
            "age3y.exe": "Age of Empires III - Asian Dynasties",
            "age3n.exe": "Age of Empires III - Napoleonic Era (MOD)",
        }.get(file, file)

    def name_to_file(self, file):
        return {
            "Age of Empires III": "age3.exe",
            "Age of Empires III - The Warchiefs": "age3x.exe",
            "Age of Empires III - Asian Dynasties": "age3y.exe",
            "Age of Empires III - Napoleonic Era (MOD)": "age3n.exe",
        }.get(file, file)
    
    def get_game_files(self, path):
        age_games = list()
        if path:
            for file in os.listdir(path):
                if file.startswith("age") and file.endswith(".exe"):
                    age_games.append(self.file_to_name(file))
        return age_games

class ConfigManager:
    root_path = ""
    game_file = ""
    interface = ""

    def __init__(self):
        self.config_file = expanduser("~") + "/aoe3force.cfg"
        logging.info("Using config file '%s'" % self.config_file)
        self.load()

    def load(self):
        if os.path.isfile(self.config_file):
            logging.info("Loading config from file '%s'" % self.config_file)
            config = configparser.ConfigParser()
            config.read(self.config_file)
            self.root_path = config["loader"]["root_dir"]
            self.game_file = config["loader"]["game_file"]
            self.interface = config["loader"]["interface"]

    def save(self):
        config = configparser.ConfigParser()
        config['loader'] = {
            'root_dir': self.root_path,
            'game_file': self.game_file,
            'interface': self.interface
            #'interface': variable.get().split(" - ")[1].strip()
        }
        with open(self.config_file, 'w') as configfile:
            config.write(configfile)


def dict_to_string_list(dict):
    return_list = list()
    for key, value in dict.items():
        return_list.append("%s - %s" % (value, key))
    return return_list



# Init Classes
ipm = IPManager()
ip_addresses = ipm.ip_addresses
ip_address_list_string = dict_to_string_list(ip_addresses)
cm = ConfigManager()
gm = GameManager(cm)


# SELECT DIRECTORY
def update_game_list():
        logging.debug("Trying to find age3*.exe files in '%s'" % cm.root_path)
        games = None
        path = cm.root_path
        if path and path != "--NO SELECTED DIRECTORY--":
            cm.root_path = path
            games = gm.get_game_files(cm.root_path)
            if games:
                menu = w2["menu"]
                menu.delete(0, "end")
                for game in games:
                    menu.add_command(label=gm.file_to_name(game), command=lambda value=gm.file_to_name(game): variable2.set(value))
                variable2.set(games[0])
        for game in gm.get_game_files(cm.root_path):
            logging.debug("Game '%s' found in selected path!" % game)
            if cm.game_file == game:
                variable2.set(game)
                break
            for int, ip in ipm.ip_addresses.items():
                if int == cm.interface:
                    variable.set("%s - %s" % (ip, int))
def update_selected_dir():
        selected_dir.configure(state=NORMAL)
        selected_dir.delete("1.0", END)
        selected_dir.insert(INSERT, cm.root_path)
        selected_dir.configure(state=DISABLED)
        cm.root_path = selected_dir.get("1.0", END).strip()
        update_game_list()
def select_dir():
    folder_selected = filedialog.askdirectory()
    if folder_selected:
        update_selected_dir()

#################
#### WINDOW LOGIC BELOW
#################
root = Tk()
root.title("AOE3 Force IP")
# IP Address Logic
variable = StringVar(root)
variable.set(ip_address_list_string[0])
w = OptionMenu(root, variable, "--NO INTERFACE SELECTED--",*ip_address_list_string)
w.pack()

b = Button(root, text="Select directory", command=select_dir)
b.pack()

selected_dir = Text(root, width="50", height="1")
selected_dir.insert(INSERT, "--NO SELECTED DIRECTORY--")
selected_dir.configure(state=DISABLED)
selected_dir.pack()

# SELECT GAME
variable2 = StringVar(root)
variable2.set("--NO PATH SELECTED YET--")
w2 = OptionMenu(root, variable2, "--NO PATH SELECTED YET--")
w2.pack()

# START GAME
b2 = Button(root, text="Start game", command=gm.start_game)
b2.pack()

# Run the program
update_selected_dir()

## Open the Window
root.mainloop()
